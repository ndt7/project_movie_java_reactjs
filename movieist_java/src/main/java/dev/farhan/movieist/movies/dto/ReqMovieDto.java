package dev.farhan.movieist.movies.dto;

import lombok.Getter;
import lombok.Setter;
@Getter @Setter
public class ReqMovieDto {
    private String imdbId;
    private String title;
    private String releaseDate;
    private String trailerLink;
    private String poster;
}
