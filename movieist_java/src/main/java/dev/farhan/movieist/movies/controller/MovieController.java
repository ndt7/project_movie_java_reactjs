package dev.farhan.movieist.movies.controller;

import dev.farhan.movieist.movies.dto.ReqMovieDto;
import dev.farhan.movieist.movies.dto.entity.Movie;
import dev.farhan.movieist.movies.service.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/movies")
@RequiredArgsConstructor
public class MovieController {
    private final MovieService service;

    @PostMapping("/create")
    public ResponseEntity<?> createMovies(@RequestBody ReqMovieDto movie) {
        return ResponseEntity.status(HttpStatus.OK).body(service.createMovies(movie));
    }

    @GetMapping
    public ResponseEntity<List<Movie>> getMovies() {
        return new ResponseEntity<>(service.findAllMovies(), HttpStatus.OK);
    }

    @GetMapping("/{imdbId}")
    public ResponseEntity<Optional<Movie>> getSingleMovie(@PathVariable String imdbId){
        return new ResponseEntity<>(service.findMovieByImdbId(imdbId), HttpStatus.OK);
    }
}
