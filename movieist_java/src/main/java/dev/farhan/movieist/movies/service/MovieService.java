package dev.farhan.movieist.movies.service;

import dev.farhan.movieist.movies.dto.ReqMovieDto;
import dev.farhan.movieist.movies.dto.entity.Movie;
import dev.farhan.movieist.movies.repository.MovieRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    private MovieRepository repository;

    public Movie createMovies(ReqMovieDto movie) {
        Movie item = new Movie();
        BeanUtils.copyProperties(movie, item);
        return repository.save(item);
    }

    public List<Movie> findAllMovies() {
        return repository.findAll();
    }
    public Optional<Movie> findMovieByImdbId(String imdbId) {
        return repository.findMovieByImdbId(imdbId);
    }
}
