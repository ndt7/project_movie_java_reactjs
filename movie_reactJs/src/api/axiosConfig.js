// config backend  db

import axios from 'axios';

export default axios.create({
    baseURL:'http://localhost:8005/',
    headers: {"ngrok-skip-browser-warning": "true"}
});
